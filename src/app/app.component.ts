import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { BookListComponent } from './shared/book-list/book-list.component';
import { BookItemComponent } from './shared/book-item/book-item.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,
  BookListComponent, BookItemComponent, RouterLink],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'angular-bibliotheque-stephen-king';
}
