import { Component, Input } from '@angular/core';
import { BookListComponent } from '../book-list/book-list.component';
import { BookDetails, BooksService } from '../../books.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-book-item',
  standalone: true,
  imports: [BookListComponent],
  templateUrl: './book-item.component.html',
  styleUrl: './book-item.component.css'
})
export class BookItemComponent {
bookItem:BookDetails | null = null
id: string | null  =null

 


constructor(private bookService : BooksService , private route : ActivatedRoute) {

}


ngOnInit(): void {
  /* je recupere l'identifiant du livre à partir de l'url  */
 const bookId = this.route.snapshot.params['id'] //stocker le resultat dans this.id
  this.id = 'works/' + bookId
 if (this.id)
  this.bookService.getBookDetails(this.id)
    .then((response: any) => {
      this.bookItem = response;
      console.log(this.bookItem)
    })
    .catch((error: any) => {
      console.error(error);
    });
}
}
