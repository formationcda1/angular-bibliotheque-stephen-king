import { Component, OnInit } from '@angular/core';
import { BookItemComponent } from '../book-item/book-item.component';
import { ActivatedRoute, RouterLink, } from '@angular/router';
import { Book, BooksService, BookDetails } from '../../books.service';

@Component({
  selector: 'app-book-list',
  standalone: true,
  imports: [BookItemComponent, RouterLink,],
  templateUrl: './book-list.component.html',
  styleUrl: './book-list.component.css'
})
export class BookListComponent {
  bookList : Book[] = []
  bookDetails:BookDetails[] = []
  constructor(private bookService : BooksService) {
    
   
  }

ngOnInit():  void {
  this.bookService.allBooks()
    .then((response: any) => {
      this.bookList = response;
      console.log(this.bookList)
      for (let eachBook of this.bookList) {
        if (eachBook.covers && eachBook.covers.length > 0 && eachBook.covers[0] != -1 ){
          console.log(eachBook.covers)
      }
      }
    })
    .catch((error: any) => {
      console.error(error);
    });
    
    console.log(this.bookList)




}

  // Définir la méthode limitDescription
  limitDescription(description: string, wordLimit: number): string {
    // Diviser la description en mots
    var words = description.split(/\s+/);

    // Sélectionner seulement le nombre de mots souhaité
    var limitedWords = words.slice(0, wordLimit);

    // Rejoindre les mots sélectionnés en une seule chaîne
    var limitedDescription = limitedWords.join(" ");

    return limitedDescription;
  }

}

