import { Routes } from '@angular/router';
import { BookItemComponent } from './shared/book-item/book-item.component';
import { BookListComponent } from './shared/book-list/book-list.component';

export const routes: Routes = [
    {path: '', component:BookListComponent,},
    {path: 'bookDetails/works/:id', component:BookItemComponent,},

    

];
