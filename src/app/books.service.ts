import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  bookList: Book[] = []; // Déclaration de la propriété bookList
  // Définition de l'URL de base de l'API Open Library
  private apiUrl = 'https://openlibrary.org/'

  async allBooks(): Promise<Book[]> {
    try {
      const response = await fetch('https://openlibrary.org/authors/OL19981A/works.json');
      const data = await response.json();
      this.bookList = data.entries;
      return this.bookList;
    } catch (error) {
      console.error('Erreur lors de la récupération des livres:', error);
      return []; // Retourne un tableau vide en cas d'erreur
    }
  }
  // Méthode pour récupérer les détails d'un livre spécifique
  async getBookDetails(key: string) {
    try {
      // Utilisation de fetch pour récupérer les détails d'un livre spécifique en utilisant sa clé depuis l'API Open Library
      const response = await fetch(`${this.apiUrl}${key}.json`);
      // Conversion de la réponse en format JSON
      const data = await response.json();
      // Retourne les détails du livre récupérés depuis l'API Open Library
      return data;
    } catch (error) {
      // En cas d'erreur, affiche l'erreur dans la console
      console.error(`Erreur lors de la récupération des détails du livre avec la clé ${key}:`, error);
      // Retourne null en cas d'erreur
      return null;
    }
  }
}

export type Book = {
  key: string,
  title: string,
  covers: number[],
  subjects: string,
  description: {
    value: string
  }
}
export type BookDetails = {
  key: string,
  title: string,
  covers: string,
  subjects: string,
  description: {
    value: string
  }
  excerpts:{
    excerp:string,
  }
  subject_places: string,

}
export type Key = {
  key: string
};